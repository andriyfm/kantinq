// Import Packages
import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import Vue2Filters from "vue2-filters";

// Import Framework7
import Framework7 from "framework7/framework7.esm.bundle.js";

// Import Framework7-Vue Plugin
import Framework7Vue from "framework7-vue/framework7-vue.esm.bundle.js";

// Import Framework7 Styles
import "framework7/css/framework7.bundle.css";

// Import Icons and App Custom Styles
import "../css/icons.css";
import "../css/app.scss";

// Import Mixin
import "./mixins";

// Import App Component
import App from "../components/app.vue";

// Init Framework7-Vue Plugin
Framework7.use(Framework7Vue);

// Init VueAxios and axios
Vue.use(VueAxios, axios);

// Init Vue2Filter
Vue.use(Vue2Filters);

// Init App
new Vue({
  el: "#app",
  render: h => h(App),

  // Register App Component
  components: {
    app: App
  }
});
